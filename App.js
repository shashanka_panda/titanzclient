import React from 'react';
import { Platform } from 'react-native';
import PropTypes from 'prop-types';
import { GiftedChat } from 'react-native-gifted-chat';
import emojiUtils from 'emoji-utils';
import Login from './Login';
import SlackMessage from './SlackMessage';
import ChatClient from './ChatClient';
import Pusher from 'pusher-js/react-native';
import pusherConfig from './pusher.json';

export default class App extends React.Component {
  constructor(props) {
    super(props); // (2)
    this.handleSubmitName = this.onSubmitName.bind(this); // (3)
  this.state = {
    messages: [],
    hasName: false,
  };
  this.pusher = new Pusher(pusherConfig.key, pusherConfig); // (1)
  
      this.chatChannel = this.pusher.subscribe('chat_channel'); // (2)
      this.chatChannel.bind('pusher:subscription_succeeded', () => { // (3)
        this.chatChannel.bind('join', (data) => { // (4)
          this.handleJoin(data.name);
        });
        this.chatChannel.bind('part', (data) => { // (5)
          this.handlePart(data.name);
        });
        this.chatChannel.bind('message', (data) => { // (6)
          this.handleMessage(data.name, data.message);
        });
      });
  
     // this.handleSendMessage = this.onSendMessage.bind(this); // (9)
}
componentDidMount() { // (7)
  fetch(`${pusherConfig.restServer}/users/${this.state.name}`, {
    method: 'PUT'
  });
}

componentWillUnmount() { // (8)
  fetch(`${pusherConfig.restServer}/users/${this.state.name}`, {
    method: 'DELETE'
  });
}

onSendMessage(text) { // (9)
  const payload = {
      message: text
  };
  fetch(`${pusherConfig.restServer}/users/${this.state.name}/messages`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  });
}

handleJoin(name) { // (4)
  const messages = this.state.messages.slice();
  messages.push({action: 'join', name: name});
  this.setState({
    messages: messages
  });
}

handlePart(name) { // (5)
  const messages = this.state.messages.slice();
  messages.push({action: 'part', name: name});
  this.setState({
    messages: messages
  });
}
onSubmitName(e) { // (5)
  const name = e.nativeEvent.text;
  this.setState({
    name: name,
    hasName: true
  });
}
  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
          },
        },
      ],
    })
  }
  handleMessage(name, message) { // (6)
    const messages = this.state.messages.slice();
    messages.push({action: 'message', name: name, message: message});
    this.setState({
      messages: messages
    });
  }
  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
    this.onSendMessage(messages);
  }

  renderMessage(props) {
    const { currentMessage: { text: currText } } = props;

    let messageTextStyle;

    // Make "pure emoji" messages much bigger than plain text.
    if (currText && emojiUtils.isPureEmojiString(currText)) {
      messageTextStyle = {
        fontSize: 28,
        // Emoji get clipped if lineHeight isn't increased; make it consistent across platforms.
        lineHeight: Platform.OS === 'android' ? 34 : 30,
      };
    }

    return (
      <SlackMessage {...props} messageTextStyle={messageTextStyle} />
    );
  }

  render() {
      if (this.state.hasName) {
        return (
         // <ChatClient name={ this.state.name } />
         <GiftedChat
         messages={this.state.messages}
         onSend={messages => this.onSend(messages)}
         user={{
           _id: 1,
         }}
         renderMessage={this.renderMessage}
       />
        );
      } else {
        return (
          <Login onSubmitName={ this.handleSubmitName } />
        );
      }
     
   
  }

}